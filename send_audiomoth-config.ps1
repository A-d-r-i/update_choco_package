$last = Read-Host -Prompt 'Last version of Audiomoth-Config'

$file = "./audiomoth-config/audiomoth-config.nuspec"
$xml = New-Object XML
$xml.Load($file)
$xml.package.metadata.version = $last
$xml.Save($file)

$Version = ([xml](Get-Content ./audiomoth-config/audiomoth-config.nuspec)).package.metadata.version

Invoke-WebRequest -Uri "https://github.com/OpenAcousticDevices/AudioMoth-Configuration-App/releases/download/$Version/AudioMothConfigurationAppSetup$Version.exe" -OutFile "./audiomoth-config/tools/AudioMothConfigurationAppSetup.exe"

cpack ./audiomoth-config/audiomoth-config.nuspec --outputdirectory .\audiomoth-config

If ($LastExitCode -eq 0) {
	choco push ./audiomoth-config/audiomoth-config.$Version.nupkg --source https://push.chocolatey.org/
} else {
 'Error'
}

Start-Sleep -Seconds 10