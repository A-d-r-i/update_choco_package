# Update choco packages

These scripts allow:

- Automate the download of the latest version of the package
- Changing the version number in the *.nuspec* file
- Push the latest version of the package to the chocolatey repository
