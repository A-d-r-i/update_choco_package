$last = Read-Host -Prompt 'Last version of Tutanota'

$file = "./tutanota/tutanota.nuspec"
$xml = New-Object XML
$xml.Load($file)
$xml.package.metadata.version = $last
$xml.Save($file)

$Version = ([xml](Get-Content ./tutanota/tutanota.nuspec)).package.metadata.version

Invoke-WebRequest -Uri "https://mail.tutanota.com/desktop/tutanota-desktop-win.exe" -OutFile "./tutanota/tools/tutanota-desktop-win.exe"

cpack ./tutanota/tutanota.nuspec --outputdirectory .\tutanota

If ($LastExitCode -eq 0) {
	choco push ./tutanota/tutanota.$Version.nupkg --source https://push.chocolatey.org/
} else {
 'Error'
}

Start-Sleep -Seconds 10
