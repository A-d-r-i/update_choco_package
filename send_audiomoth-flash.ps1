$last = Read-Host -Prompt 'Last version of Audiomoth-Flash'

$file = "./audiomoth-flash/audiomoth-flash.nuspec"
$xml = New-Object XML
$xml.Load($file)
$xml.package.metadata.version = $last
$xml.Save($file)

$Version = ([xml](Get-Content ./audiomoth-flash/audiomoth-flash.nuspec)).package.metadata.version

Invoke-WebRequest -Uri "https://github.com/OpenAcousticDevices/AudioMoth-Flash-App/releases/download/$Version/AudioMothFlashAppSetup$Version.exe" -OutFile "./audiomoth-flash/tools/AudioMothFlashAppSetup.exe"

cpack ./audiomoth-flash/audiomoth-flash.nuspec --outputdirectory .\audiomoth-flash

If ($LastExitCode -eq 0) {
	choco push ./audiomoth-flash/audiomoth-flash.$Version.nupkg --source https://push.chocolatey.org/
} else {
 'Error'
}

Start-Sleep -Seconds 10