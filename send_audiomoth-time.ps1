$last = Read-Host -Prompt 'Last version of Audiomoth-Time'

$file = "./audiomoth-time/audiomoth-time.nuspec"
$xml = New-Object XML
$xml.Load($file)
$xml.package.metadata.version = $last
$xml.Save($file)

$Version = ([xml](Get-Content ./audiomoth-time/audiomoth-time.nuspec)).package.metadata.version

Invoke-WebRequest -Uri "https://github.com/OpenAcousticDevices/AudioMoth-Time-App/releases/download/$Version/AudioMothTimeAppSetup$Version.exe" -OutFile "./audiomoth-time/tools/AudioMothTimeAppSetup.exe"

cpack ./audiomoth-time/audiomoth-time.nuspec --outputdirectory .\audiomoth-time

If ($LastExitCode -eq 0) {
	choco push ./audiomoth-time/audiomoth-time.$Version.nupkg --source https://push.chocolatey.org/
} else {
 'Error'
}

Start-Sleep -Seconds 10
