$last = Read-Host -Prompt 'Last version of Raven'

$file = "./raven/raven.nuspec"
$xml = New-Object XML
$xml.Load($file)
$xml.package.metadata.version = $last
$xml.Save($file)

$Version = ([xml](Get-Content ./raven/raven.nuspec)).package.metadata.version

Invoke-WebRequest -Uri "https://github.com/hello-efficiency-inc/raven-reader/releases/download/v$Version/Raven-Reader-Setup-$Version.exe" -OutFile "raven.exe"

$TABLE = Get-FileHash raven.exe -Algorithm SHA256
$SHA = $TABLE.Hash

$content = "`$packageName = 'raven'
`$installerType = 'EXE'
`$url = 'https://github.com/hello-efficiency-inc/raven-reader/releases/download/v$Version/Raven-Reader-Setup-$Version.exe'
`$checksum = '$SHA'
`$checkumType = 'sha256'
`$silentArgs = '/S'
`$validExitCodes = @(0)
Install-ChocolateyPackage `"`$packageName`" `"`$installerType`" `"`$silentArgs`" `"`$url`" -checksum `$checksum -checksumType `$checkumType -validExitCodes `$validExitCodes " | out-file -filepath ./raven/tools/chocolateyinstall.ps1

Remove-Item raven.exe

cpack ./raven/raven.nuspec --outputdirectory .\raven

If ($LastExitCode -eq 0) {
	choco push ./raven/raven.$Version.nupkg --source https://push.chocolatey.org/
} else {
 'Error'
}

Start-Sleep -Seconds 10