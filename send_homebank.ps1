$last = Read-Host -Prompt 'Last version of HomeBank'

$file = "./homebank/homebank.nuspec"
$xml = New-Object XML
$xml.Load($file)
$xml.package.metadata.version = $last
$xml.Save($file)

$Version = ([xml](Get-Content ./homebank/homebank.nuspec)).package.metadata.version

Invoke-WebRequest -Uri "http://homebank.free.fr/public/HomeBank-$last-setup.exe" -OutFile "homebank.exe"

$TABLE = Get-FileHash homebank.exe -Algorithm SHA256
$SHA = $TABLE.Hash

$content = "`$packageName = 'homebank'
`$installerType = 'EXE'
`$url = 'http://homebank.free.fr/public/HomeBank-$version-setup.exe'
`$checksum = '$SHA'
`$checkumType = 'sha256'
`$silentArgs = '/verysilent /allusers'
`$validExitCodes = @(0)
Install-ChocolateyPackage `"`$packageName`" `"`$installerType`" `"`$silentArgs`" `"`$url`" -checksum `$checksum -checksumType `$checkumType -validExitCodes `$validExitCodes " | out-file -filepath ./homebank/tools/chocolateyinstall.ps1

Remove-Item homebank.exe

cpack ./homebank/homebank.nuspec --outputdirectory .\homebank

If ($LastExitCode -eq 0) {
	choco push ./homebank/homebank.$Version.nupkg --source https://push.chocolatey.org/
} else {
 'Error'
}

Start-Sleep -Seconds 10